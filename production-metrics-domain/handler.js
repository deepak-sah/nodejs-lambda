

productionMetricsHandler = (request, response) => {
    
    // The input data recevied from the user
    const payload = request.body;
    
    // Validate the input data
    try {
        productionMetrics.validatePayload(payload);
        
        // Create a payload to pass to .NET Lambda function
        const inputPayload = productionMetrics.buildPayload(payload);
        
        // Send the API request to the .NET lambda functiont
        client.post(env.spreadsheetApi.url, inputPayload).then((res) => {
            response({
                statusCode: 200,
                headers: {'Access-Control-Allow-Origin':'*', 'Access-Control-Allow-Headers':'*'},
                body: productionMetrics.buildProductionResponse(res.data,payload)
            });
        })
    } catch(error) {
        
        response({
            
            statusCode: Constants.Http.StatusCodes.BadRequest,
            headers: { 'Content-Type' : Constants.Http.Headers.ContentType },
            body: { Response: Constants.Http.Respones.BadRequest }
        });
    }
}