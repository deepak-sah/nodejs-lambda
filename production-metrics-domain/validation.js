'use strict';

var _tcomb = require('tcomb');

var _tcomb2 = _interopRequireDefault(_tcomb);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

var ProductionMetricsInputJson = _tcomb2.default.struct({
    production: _tcomb2.default.struct({

        mcfeToBoeRatio: _tcomb2.default.Number,
        tilMonth: _tcomb2.default.Number,
        wellLife: _tcomb2.default.Number,
        wellLifeMonths: _tcomb2.default.Number,
        oilIp: _tcomb2.default.Number,
        oilDi: _tcomb2.default.Number,
        oilBi: _tcomb2.default.Number,
        oilDMin: _tcomb2.default.Number,
        oilDMinMonthlyEquivalent: _tcomb2.default.Number,
        wellheadGasIP: _tcomb2.default.Number,
        wellheadGasDi: _tcomb2.default.Number,
        wellheadGasBi: _tcomb2.default.Number,
        wellheadGasDMin: _tcomb2.default.Number,
        wellheadGasDMinMonthlyEquivalent: _tcomb2.default.Number,
        NGLYield: _tcomb2.default.Number,
        nglIp: _tcomb2.default.Number,
        dryGasIp: _tcomb2.default.Number,
        gasProcessed: _tcomb2.default.Number,
        processingPlantEfficiency: _tcomb2.default.Number,
        waterIP: _tcomb2.default.Number,
        waterDi: _tcomb2.default.Number,
        waterBi: _tcomb2.default.Number,
        waterDMin: _tcomb2.default.Number,
        waterDMinMonthlyEquivalent: _tcomb2.default.Number,
        forecastYear: _tcomb2.default.Number

        // tilMonth: _tcomb2.default.Number,
        // wellLife: _tcomb2.default.Number,
        // oilIp: _tcomb2.default.Number,
        // oilDi: _tcomb2.default.Number,
        // oilBi: _tcomb2.default.Number,
        // oilDMin: _tcomb2.default.Number,
        // wellheadGasIP: _tcomb2.default.Number,
        // wellheadGasDi: _tcomb2.default.Number,
        // wellheadGasBi: _tcomb2.default.Number,
        // wellheadGasDMin: _tcomb2.default.Number,
        // gasProcessed: _tcomb2.default.Number,
        // NGLYield: _tcomb2.default.Number,
        // processingPlantEfficiency: _tcomb2.default.Number,
        // waterIP: _tcomb2.default.Number,
        // waterDi: _tcomb2.default.Number,
        // waterBi: _tcomb2.default.Number,
        // waterDMin: _tcomb2.default.Number
    })//,

    // wellDesign: _tcomb2.default.struct({
    //     drillingDays: _tcomb2.default.Number,
    //     stringOfCassing: _tcomb2.default.Number,
    //     measuredDepth: _tcomb2.default.Number,
    //     lateralLength: _tcomb2.default.Number,
    //     proppant: _tcomb2.default.Number,
    //     fracStage: _tcomb2.default.Number
    // })
}, { name: 'ProductionMetricsInputJson', strict: true });

function validatePayload(payload) {
    var production = payload.production;//,
        // wellDesign = payload.wellDesign;
    console.log('Validating...');

    try {
        ProductionMetricsInputJson({
            production: {

                mcfeToBoeRatio: parseFloat(production.mcfeToBoeRatio),
                tilMonth: parseFloat(production.tilMonth),
                wellLife: parseFloat(production.wellLife),
                wellLifeMonths: parseFloat(production.wellLifeMonths),
                oilIp: parseFloat(production.oilIp),
                oilDi: parseFloat(production.oilDi),
                oilBi: parseFloat(production.oilBi),
                oilDMin: parseFloat(production.oilDMin),
                oilDMinMonthlyEquivalent: parseFloat(production.oilDMinMonthlyEquivalent),
                wellheadGasIP: parseFloat(production.wellheadGasIP),
                wellheadGasDi: parseFloat(production.wellheadGasDi),
                wellheadGasBi: parseFloat(production.wellheadGasBi),
                wellheadGasDMin: parseFloat(production.wellheadGasDMin),
                wellheadGasDMinMonthlyEquivalent: parseFloat(production.wellheadGasDMinMonthlyEquivalent),
                NGLYield: parseFloat(production.NGLYield),
                nglIp: parseFloat(production.nglIp),
                dryGasIp: parseFloat(production.dryGasIp),
                gasProcessed: parseFloat(production.gasProcessed),
                processingPlantEfficiency: parseFloat(production.processingPlantEfficiency),
                waterIP: parseFloat(production.waterIP),
                waterDi: parseFloat(production.waterDi),
                waterBi: parseFloat(production.waterBi),
                waterDMin: parseFloat(production.waterDMin),
                waterDMinMonthlyEquivalent: parseFloat(production.waterDMinMonthlyEquivalent),
                forecastYear: parseFloat(production.forecastYear)

                // tilMonth: parseFloat(production.tilMonth),
                // wellLife: parseFloat(production.wellLife),
                // oilIp: parseFloat(production.oilIp),
                // oilDi: parseFloat(production.oilDi),
                // oilBi: parseFloat(production.oilBi),
                // oilDMin: parseFloat(production.oilDMin),
                // wellheadGasIP: parseFloat(production.wellheadGasIP),
                // wellheadGasDi: parseFloat(production.wellheadGasDi),
                // wellheadGasBi: parseFloat(production.wellheadGasBi),
                // wellheadGasDMin: parseFloat(production.wellheadGasDMin),
                // gasProcessed: parseFloat(production.gasProcessed),
                // NGLYield: parseFloat(production.NGLYield),
                // processingPlantEfficiency: parseFloat(production.processingPlantEfficiency),
                // waterIP: parseFloat(production.waterIP),
                // waterDi: parseFloat(production.waterDi),
                // waterBi: parseFloat(production.waterBi),
                // waterDMin: parseFloat(production.waterDMin)
            }//,

            // wellDesign: {
            //     drillingDays: parseFloat(wellDesign.drillingDays),
            //     stringOfCassing: parseFloat(wellDesign.stringOfCassing),
            //     measuredDepth: parseFloat(wellDesign.measuredDepth),
            //     lateralLength: parseFloat(wellDesign.lateralLength),
            //     proppant: parseFloat(wellDesign.proppant),
            //     fracStage: parseFloat(wellDesign.fracStage)
            // }
        });

        return true;
    } catch (error) {
        console.error(error);
        // return false;
        throw { message: "Validation error occurred: " };
    }
}

module.exports = { validatePayload: validatePayload };
