var config = require('../config.json');

buildPayload = (payload) => {
    // Load the configuration for Production Metrics
    const productionMetricsConfig = config.productionMetrics;

    const jsonObj = {      
        workbook: {
            spreadsheetgearLicense: productionMetricsConfig.spreadsheetgearLicense,
            workbookName: productionMetricsConfig.workbook,
            worksheetName: productionMetricsConfig.worksheet,
            s3BucketName: productionMetricsConfig.bucket
        } 
    };

    const inputDataMapping = productionMetricsConfig.inputCellMapping;
    jsonObj.inputDataMapping = new Array();

    const production = payload.production;
    //const wellDesign = payload.wellDesign;

    // Iterate through the Input cell mappings and build the JSON object
    for (const key of Object.keys(production)) {

        if (key === "wellLifeMonths" ||
             key === "oilDMinMonthlyEquivalent" ||
              key === "wellheadGasDMinMonthlyEquivalent" ||
               key === "waterDMinMonthlyEquivalent") {
                 console.log("ZXC");
                  continue;
        }

        let cellValue = production[key];

        if (key === "oilDMin" ||
             key === "wellheadGasDMin" ||
              key === "gasProcessed" ||
               key === "waterDMin" ||
                key === "processingPlantEfficiency") {
                 //console.log("ZXC");
                //  continue;
                cellValue /= 100;
        }

        let tempObj = {
            cell: inputDataMapping[key],
            value: cellValue //production[key]
        };

        // let tempObj = {
        //     cell: inputDataMapping[key],
        //     value: production[key]
        // };

        jsonObj.inputDataMapping.push(tempObj);
    }

    /*for (const key of Object.keys(wellDesign)) {
        let tempObj = {
            cell: inputCellMapping[key],
            value: production[key]
        };

        jsonObj.inputCellMapping.push(tempObj);
    }*/

    const outputDataMapping = productionMetricsConfig.outputCellMapping;
    const continuousOutputData = outputDataMapping.continuousOutputData;
    const discreteOutputData = outputDataMapping.discreteOutputData;

    jsonObj.outputDataMapping = {
        continuousOutputData: [],
        discreteOutputData: []
    };

    const objArray = new Array();
    const newArray = new Array();

    continuousOutputData.forEach((item) => {
        const tempObj = item;
        const wellLife = production.wellLife;
        tempObj.columnEnd = `${wellLife * 12 + 5}`;
        objArray.push(tempObj);
    });

    discreteOutputData.forEach((item) => {
        newArray.push(item);
    });

    jsonObj.outputDataMapping = {
        continuousOutputData: objArray,
        discreteOutputData: newArray
    }

    /*jsonObj.outputCellMapping.continuousOutputData.forEach((item) => {
        item.columnEnd = 30;
    });*/

    return jsonObj;
};

module.exports = { buildPayload: buildPayload };