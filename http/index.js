/**
 * Contain methods for required HTTP methods.
 */

'use strict';

const Utils = require('../utils');

var _axios = require('axios');
var _axios2 = Utils._interopRequireDefault(_axios);

/**
 * Performs a HTTP POST operation on the specified API endpoint.
 * @param {*} api the API endpoint to perform the POST request on
 * @param {*} payload the request to be sent
 */
var post = function post(api, payload) {
    
    return new Promise((resolve,reject) => {
       
        _axios2.default.post(api, payload).then(function (result) {
            
            console.log('POST method succeeded');
            resolve(result);
        }).catch(function (error) {
            
            console.log('POST method failed');
            reject(error);
        })
    })
};

module.exports = {
    post: post
};
