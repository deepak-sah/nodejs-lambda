const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: './transpiledIndex.js',
    mode: 'development',
    target: 'node',
    output: {
        libraryTarget: 'commonjs',
        path: path.resolve(__dirname, 'dist'),
        filename: 'transpiledIndex.js', // this should match the first part of function handler in serverless.yml
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            }
        ]
    },
    plugins: [
        new CopyWebpackPlugin(
            [
                'template.yml',
                'config.json'
            ],
        ),
        new webpack.DefinePlugin({
            PRODUCTION: JSON.stringify(true)
        })
    ],
    devServer: {
        port: 7700
    }
};