import Alpr from 'alpr';
// import request from 'alpr/request';

const Constants = require('./constants');

const env = require('./config.json');
const Metrics = require('./metrics');
// const Production = require('./metrics/production')
const Handler = require('./api-request-handler');

/**
 * Lambda handler function that processes user requests to calculate and return
 * well data.
 * @param {*} event the AWS event to handle
 * @param {*} context the AWS Lambda context
 * @param {*} callback the callback to execute
 */
exports.handler = function handler(event, context, callback) {
    const alpr = new Alpr({ event, context, callback });

    // API route for getting the production data

    // OPTIONS HTTP method is needed to allow CORS
    alpr.route({
        method: 'OPTIONS',
        path: '/production/',
        handler: (request, response) => {
            console.log('Before OPTIONS');
            response({
                statusCode: Constants.Http.StatusCodes.OK,
                headers: {
                    'Access-Control-Allow-Origin' : Constants.Http.Headers.AccessControlAllowOrigin,
                    'Access-Control-Allow-Methods' : Constants.Http.Headers.AccessControlAllowMethods,
                    'Access-Control-Allow-Headers' : Constants.Http.Headers.AccessControlAllowHeaders,
                    'Content-Type' : Constants.Http.Headers.ContentType
                },
                body: {}
            });
        }
    });

    // POST HTTP method for this route is the endpoint for getting the JSON data for production
    alpr.route({
        method: 'POST',
        path: '/production/',
        handler: (request, response) => {
            Handler.apiRequestHandler(env.spreadsheetApi.url, request, response, Metrics.Production.validatePayload, Metrics.Production.buildPayload, Metrics.Production.buildResponse)
        }
    });

    // If an invalid route is accessed, return an error
    if (!alpr.routeMatched) {
        
        callback({
            
            statusCode: Constants.Http.StatusCodes.NotFound,
            headers: { 'Content-Type' : Constants.Http.Headers.ContentType },
            body: { Response: Constants.Http.Responses.NotFound }
        });
    }
}