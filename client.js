'use strict';

var _axios = require('axios');
var _axios2 = _interopRequireDefault(_axios);

const producionMetrics = require('./production-metrics-domain')

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

// POST call
var post = function post(api, payload) {
    // make a post call to the c# api here
    return new Promise((resolve,reject)=>{
       _axios2.default.post(api, payload)
       .then(function (res) {
            console.log('API Passed');
            resolve(res);
        })
        .catch(function (error) {
            console.log('API Failed');
            reject(error);
        })
    })
};

var get = function get(queryParameters, api) {
    // make a get call to the c# api here
};

module.exports = {
    post: post,
    get: get
};
