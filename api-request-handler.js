const Constants = require('./constants');
const Http = require('./http');

/**
 * This method handles a client's API request for required metrics (e.e.g, production, revenue, etc.).
 * @param {*} url the endpoint to send the API request to 
 * @param {*} request the HTTP request object sent by the client
 * @param {*} response the callback used to create the HTTP response object
 * @param {*} payloadValidator the callback to validate the input payload
 * @param {*} payloadBuilder the callback to build the input payload that is sent to the .NET backend to retrieve the metrics from the MS Excel sheet
 * @param {*} responseBuilder the callback to build the output JSON object that is sent back to the client 
 */
apiRequestHandler = (url, request, response, payloadValidator, payloadBuilder, responseBuilder) => {
    
    // The user input data receivied from the client
    const payload = request.body;
    
    try {

        //console.log('Input JSON: ' + JSON.stringify(payload));
        
        //console.log('Before validation');
        
        // Validate the input data
        payloadValidator(payload);
        //console.log('After validation');
        
        //console.log('Before payload building');
        // Create a payload to pass to .NET Lambda function
        const inputPayload = payloadBuilder(payload);
        //console.log('After payload building');
        
        // Send the API request to the .NET lambda function
        // The original JSON input (payload) received from the client is also
        // forwarded to the resposneBuilder() method because in some cases,
        // the original input data is required for certain steps (e.g., the
        // well life in months is required for production).
        Http.post(url, inputPayload).then((result) => {
            
            response({
                statusCode: Constants.Http.StatusCodes.OK,
                headers: {
                    'Access-Control-Allow-Origin': Constants.Http.Headers.AccessControlAllowOrigin,
                    'Access-Control-Allow-Headers': Constants.Http.Headers.AccessControlAllowHeaders,
                    'Content-Type' : Constants.Http.Headers.ContentType
                },
                body: responseBuilder(result.data, payload)
            });
        })
    } catch(error) {
        
        response({
            
            statusCode: Constants.Http.StatusCodes.BadRequest,
            headers: { 'Content-Type' : Constants.Http.Headers.ContentType },
            body: { Response: Constants.Http.Responses.BadRequest }
        });
    }
}

module.exports = {
    apiRequestHandler: apiRequestHandler
}