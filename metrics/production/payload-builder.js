var config = require('./config.json');

buildPayload = (payload) => {

    // Load the configuration for Production Metrics
    const productionMetricsConfig = config.productionMetrics;

    const jsonObj = {      
        workbook: {
            workbookName: productionMetricsConfig.workbook,
            worksheetName: productionMetricsConfig.worksheet,
            s3BucketName: productionMetricsConfig.bucket
        } 
    };

    const inputDataMapping = productionMetricsConfig.inputCellMapping;
    jsonObj.inputDataMapping = new Array();

    const production = payload.production;

    // Iterate through the Input cell mapping, output cell mapping and build the JSON object
    
    for (const key of Object.keys(production)) {

        let cellValue = production[key];

        if (key === "oilDMin" ||
             key === "wellheadGasDMin" ||
              key === "gasProcessed" ||
               key === "waterDMin" ||
                key === "processingPlantEfficiency") {
            cellValue /= 100;
        }

        let tempObj = {
            cell: inputDataMapping[key],
            value: cellValue
        };

        jsonObj.inputDataMapping.push(tempObj);
    }

    const outputDataMapping = productionMetricsConfig.outputCellMapping;
    const continuousOutputData = outputDataMapping.continuousOutputData;
    const discreteOutputData = outputDataMapping.discreteOutputData;

    jsonObj.outputDataMapping = {
        continuousOutputData: [],
        discreteOutputData: []
    };

    const objArray = new Array();
    const newArray = new Array();

    continuousOutputData.forEach((item) => {
        const tempObj = item;
        const wellLife = production.wellLife;
        tempObj.columnEnd = `${wellLife * 12 + 5}`;
        objArray.push(tempObj);
    });

    discreteOutputData.forEach((item) => {
        newArray.push(item);
    });

    jsonObj.outputDataMapping = {
        continuousOutputData: objArray,
        discreteOutputData: newArray
    }

    return jsonObj;
};

module.exports = { 
    buildPayload: buildPayload
};