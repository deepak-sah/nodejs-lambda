const validatePayload = require('./payload-validator').validatePayload;
const buildPayload = require('./payload-builder').buildPayload;
const buildResponse = require('./response-builder').buildResponse;

module.exports = {
    validatePayload: validatePayload,
    buildPayload: buildPayload,
    buildResponse: buildResponse
};
