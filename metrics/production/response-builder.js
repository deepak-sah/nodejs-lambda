const config = require('./config.json');

// Constants for accessing the output JSON object

const THIRTY_DAY_IP_OIL_EQUIVALENT = 0;
const THIRTY_DAY_IP_GAS_EQUIVALENT = 1;
const WATER_EUR = 2;
const OIL_EUR = 3;
const NGL_EUR = 4;
const DRY_GAS_EUR = 5;
const OIL_EUR_PERCENTAGE = 6;
const NGL_EUR_PERCENTAGE = 7;
const DRY_GAS_PERCENTAGE = 8;
const GAS_SHRINKAGE_PERCENTAGE = 9;
const OIL_THIRTY_DAY_IP = 10;
const OIL_DI = 11;
const OIL_BI = 12;
const OIL_DMIN = 13;
const WH_GAS_THIRTY_DAY_IP = 14;
const WH_GAS_DI = 15;
const WH_GAS_BI = 16;
const WH_GAS_DMIN = 17;
const NGL_THIRTY_DAY_IP = 18;
const NGL_YIELD = 19;
const DRY_GAS_THIRTY_DAY_IP = 20;
const GAS_SHRINKAGE = 21;
const GAS_PROCESSED = 22;
const PLANT_EFFECTIVENESS = 23;
const WATER_THIRTY_DAY_IP = 24;
const WATER_DI = 25;
const WATER_BI = 26;
const WATER_DMIN = 27;
const OIL_HYPERBOLIC_PERCENTAGE_DECLINE = 28;
const OIL_MOM_PERCENTAGE_DECLINE = 29;
const OIL_TIME_ZERO_UNCONSTRAINED_PRODUCTION = 30;
const OIL_EUR_WELL_PRODUCTION_TIME_ZERO = 31;
const OIL_ACTUAL_PRODUCTION_BARRELS_PER_DAY = 32;
const OIL_CUMULATIVE_ACTUAL_PRODUCTION = 33;
const OIL_ACTUAL_PRODUCTION_MMFCE = 34;
const WH_GAS_HYPERBOLIC_PERCENTAGE_DECLINE = 35;
const WH_GAS_MOM_PERCENTAGE_DECLINE = 36;
const WH_GAS_TIME_ZERO_UNCONSTRAINED_PRODUCTION = 37;
const WH_GAS_EUR_WELL_PRODUCTION_TIME_ZERO = 38;
const WH_GAS_ACTUAL_PRODUCTION_MMFCD = 39;
const WH_GAS_CUMULATIVE_ACTUAL_PRODUCTION = 40;
const NGL_TIME_ZERO_UNCONSTRAINED_PRODUCTION = 41;
const NGL_EUR_WELL_PRODUCTION_TIME_ZERO = 42;
const NGL_ACTUAL_PRODUCTION_BARRELS_PER_DAY = 43;
const NGL_CUMULATIVE_ACTUAL_PRODUCTION = 44;
const DRY_GAS_TIME_ZERO_UNCONSTRAINED_PRODUCTION = 45;
const DRY_GAS_EUR_WELL_PRODUCTION_TIME_ZERO = 46;
const DRY_GAS_ACTUAL_PRODUCTION_MMCFED = 47;
const DRY_GAS_CUMULATIVE_ACTUAL_PRODUCTION = 48;
const WATER_HYPERBOLIC_PERCENTAGE_DECLINE = 49;
const WATER_MOM_PERCENTAGE_DECLINE = 50;
const WATER_TIME_ZERO_UNCONSTRAINED_PRODUCTION = 51;
const WATER_ACTUAL_PRODUCTION_BARRELS_PER_DAY = 52;
const WATER_CUMULATIVE_ACTUAL_PRODUCTION = 53;
const TOTAL_ACTUAL_PRODUCTION_BOED = 54;
const TOTAL_ACTUAL_PRODUCTION_MMFCED = 55;
const CUMULATIVE_TOTAL_PRODUCTION_MBOE = 56;
const CUMULATIVE_TOTAL_PRODUCTION_BCFE = 57;

buildResponse = (payload, clientPayload) => {

    // Load the configuration for series data
    const seriesConfig = config.series;
    const summaryConfig = config.summary;

    const responseJson = {      
        series: seriesConfig,
        summary: summaryConfig
    };

    const months = clientPayload.production.wellLifeMonths;
    const quarters = months / 3;
    const years = months / 12;
    const baseYear = 2018;

    getMonthlyData(payload, months, responseJson);
    getQuarterlyData(payload, months, baseYear, responseJson);
    getYearlyData(payload, months, baseYear, responseJson);
    getSummaryData(payload, responseJson);

    return responseJson;
};

getMonthlyData = (payload, months, responseJson) => {

    const wellLifeMonths = months;
    var monthlyData = responseJson.series.monthly;
    // var outputData = payload.output;

    //responseJson.series.monthly.oilHyperbolicPercentageDecline = payload.output[OIL_HYPERBOLIC_PERCENTAGE_DECLINE].oilHyperbolicPercentageDecline;

    for (var i=1; i <= wellLifeMonths; ++i) {
        //responseJson.series.monthly["month"].push(i);
        //responseJson.series.monthly[Object.keys(monthlyData)[0]].push(i);

        // for (var property=1; property < 30; ++property) {

        //     responseJson.series.monthly[Object.keys(monthlyData)[property]].push(payload.output[property+OIL_HYPERBOLIC_PERCENTAGE_DECLINE][Object.keys(monthlyData)[property]][i]);
        // }
        
        //responseJson.series.monthly.Object.keys(monthlyData)[1].push(payload.output[OIL_HYPERBOLIC_PERCENTAGE_DECLINE].oilHyperbolicPercentageDecline[i]);
        monthlyData.month.push(i);
        monthlyData.oilHyperbolicPercentageDecline.push(payload.output[OIL_HYPERBOLIC_PERCENTAGE_DECLINE].oilHyperbolicPercentageDecline[i]);
        monthlyData.oilMomPercentageDecline.push(payload.output[OIL_MOM_PERCENTAGE_DECLINE].oilMomPercentageDecline[i]);
        monthlyData.oilTimeZeroUnconstrainedProduction.push(payload.output[OIL_TIME_ZERO_UNCONSTRAINED_PRODUCTION].oilTimeZeroUnconstrainedProduction[i]);
        monthlyData.oilEurWellProductionTimeZero.push(payload.output[OIL_EUR_WELL_PRODUCTION_TIME_ZERO].oilEurWellProductionTimeZero[i]);
        monthlyData.oilActualProductionBarrelsPerDay.push(payload.output[OIL_ACTUAL_PRODUCTION_BARRELS_PER_DAY].oilActualProductionBarrelsPerDay[i]);
        monthlyData.oilCumulativeActualProduction.push(payload.output[OIL_CUMULATIVE_ACTUAL_PRODUCTION].oilCumulativeActualProduction[i]);
        monthlyData.oilActualProductionMMFCE.push(payload.output[OIL_ACTUAL_PRODUCTION_MMFCE].oilActualProductionMMFCE[i]);
        monthlyData.WHGasHyperbolicPercentageDecline.push(payload.output[WH_GAS_HYPERBOLIC_PERCENTAGE_DECLINE].WHGasHyperbolicPercentageDecline[i]);
        monthlyData.WHGasMomPercentageDecline.push(payload.output[WH_GAS_MOM_PERCENTAGE_DECLINE].WHGasMomPercentageDecline[i]);
        monthlyData.WHGasTimeZeroUnconstrainedProduction.push(payload.output[WH_GAS_TIME_ZERO_UNCONSTRAINED_PRODUCTION].WHGasTimeZeroUnconstrainedProduction[i]);
        monthlyData.WHGasEurWellProductionTimeZero.push(payload.output[WH_GAS_EUR_WELL_PRODUCTION_TIME_ZERO].WHGasEurWellProductionTimeZero[i]);
        monthlyData.WHGasActualProductionMMFCD.push(payload.output[WH_GAS_ACTUAL_PRODUCTION_MMFCD].WHGasActualProductionMMFCD[i]);
        monthlyData.WHGasCumulativeActualProduction.push(payload.output[WH_GAS_CUMULATIVE_ACTUAL_PRODUCTION].WHGasCumulativeActualProduction[i]);
        monthlyData.NGLTimeZeroUnconstrainedProduction.push(payload.output[NGL_TIME_ZERO_UNCONSTRAINED_PRODUCTION].NGLTimeZeroUnconstrainedProduction[i]);
        monthlyData.NGLEurWellProductionTimeZero.push(payload.output[NGL_EUR_WELL_PRODUCTION_TIME_ZERO].NGLEurWellProductionTimeZero[i]);
        monthlyData.NGLActualProductionBarrelsPerDay.push(payload.output[NGL_ACTUAL_PRODUCTION_BARRELS_PER_DAY].NGLActualProductionBarrelsPerDay[i]);
        monthlyData.NGLCumulativeActualProduction.push(payload.output[NGL_CUMULATIVE_ACTUAL_PRODUCTION].NGLCumulativeActualProduction[i]);
        monthlyData.dryGasTimeZeroUnconstrainedProduction.push(payload.output[DRY_GAS_TIME_ZERO_UNCONSTRAINED_PRODUCTION].dryGasTimeZeroUnconstrainedProduction[i]);
        monthlyData.dryGasEurWellProductionTimeZero.push(payload.output[DRY_GAS_EUR_WELL_PRODUCTION_TIME_ZERO].dryGasEurWellProductionTimeZero[i]);
        monthlyData.dryGasActualProductionMMCFED.push(payload.output[DRY_GAS_ACTUAL_PRODUCTION_MMCFED].dryGasActualProductionMMCFED[i]);
        monthlyData.dryGasCumulativeActualProduction.push(payload.output[DRY_GAS_CUMULATIVE_ACTUAL_PRODUCTION].dryGasCumulativeActualProduction[i]);
        monthlyData.waterHyperbolicPercentageDecline.push(payload.output[WATER_HYPERBOLIC_PERCENTAGE_DECLINE].waterHyperbolicPercentageDecline[i]);
        monthlyData.waterMomPercentageDecline.push(payload.output[WATER_MOM_PERCENTAGE_DECLINE].waterMomPercentageDecline[i]);
        monthlyData.waterTimeZeroUnconstrainedProduction.push(payload.output[WATER_TIME_ZERO_UNCONSTRAINED_PRODUCTION].waterTimeZeroUnconstrainedProduction[i]);
        monthlyData.waterActualProductionBarrelsPerDay.push(payload.output[WATER_ACTUAL_PRODUCTION_BARRELS_PER_DAY].waterActualProductionBarrelsPerDay[i]);
        monthlyData.waterCumulativeActualProduction.push(payload.output[WATER_CUMULATIVE_ACTUAL_PRODUCTION].waterCumulativeActualProduction[i]);
        monthlyData.totalActualProductionBOED.push(payload.output[TOTAL_ACTUAL_PRODUCTION_BOED].totalActualProductionBOED[i]);
        monthlyData.totalActualproductionMMFCED.push(payload.output[TOTAL_ACTUAL_PRODUCTION_MMFCED].totalActualproductionMMFCED[i]);
        monthlyData.cumulativeTotalProductionMBOE.push(payload.output[CUMULATIVE_TOTAL_PRODUCTION_MBOE].cumulativeTotalProductionMBOE[i]);
        monthlyData.cumulativeTotalProductionBCFE.push(payload.output[CUMULATIVE_TOTAL_PRODUCTION_BCFE].cumulativeTotalProductionBCFE[i]);
    }

    return responseJson;
};

getQuarterlyData = (payload, months, baseYear, responseJson) => {

    const wellLifeMonths = months;
    for (var i=1; i <= wellLifeMonths / 3; ++i) {
        responseJson.series.quarterly.quarter.push( `Q${(i-1)%4+1}-${Math.floor(baseYear+(i-1)/4)}` );
    }

    for (var i=0; i < wellLifeMonths; i+=3) {
        
        let oilHyperbolicPercentageDeclineSum = 0;
        let oilMomPercentageDeclineSum = 0;
        let oilTimeZeroUnconstrainedProductionSum  = 0;
        let oilEurWellProductionTimeZeroSum = 0;
        let oilActualProductionBarrelsPerDaySum = 0;
        let oilCumulativeActualProductionSum = 0;
        let oilActualProductionMMFCESum = 0;
        let WHGasHyperbolicPercentageDeclineSum = 0;
        let WHGasMomPercentageDeclineSum = 0;
        let WHGasTimeZeroUnconstrainedProductionSum = 0;
        let WHGasEurWellProductionTimeZeroSum = 0;
        let WHGasActualProductionMMFCDSum = 0;
        let WHGasCumulativeActualProductionSum = 0;
        let NGLTimeZeroUnconstrainedProductionSum = 0;
        let NGLEurWellProductionTimeZeroSum = 0;
        let NGLActualProductionBarrelsPerDaySum = 0;
        let NGLCumulativeActualProductionSum = 0;
        let dryGasTimeZeroUnconstrainedProductionSum = 0;
        let dryGasEurWellProductionTimeZeroSum = 0;
        let dryGasActualProductionMMCFEDSum = 0;
        let dryGasCumulativeActualProductionSum = 0;
        let waterHyperbolicPercentageDeclineSum = 0;
        let waterMomPercentageDeclineSum = 0;
        let waterTimeZeroUnconstrainedProductionSum = 0;
        let waterActualProductionBarrelsPerDaySum = 0;
        let waterCumulativeActualProductionSum = 0;
        let totalActualProductionBOEDSum = 0;
        let totalActualproductionMMFCEDSum = 0;
        let cumulativeTotalProductionMBOESum = 0;
        let cumulativeTotalProductionBCFESum = 0;

        //console.log("B" + i)
        
        for (var j=i; j < i+3; ++j) {
            
            oilHyperbolicPercentageDeclineSum += payload.output[OIL_HYPERBOLIC_PERCENTAGE_DECLINE].oilHyperbolicPercentageDecline[j];
            oilMomPercentageDeclineSum += payload.output[OIL_MOM_PERCENTAGE_DECLINE].oilMomPercentageDecline[j];
            oilTimeZeroUnconstrainedProductionSum += payload.output[OIL_TIME_ZERO_UNCONSTRAINED_PRODUCTION].oilTimeZeroUnconstrainedProduction[j];
            oilEurWellProductionTimeZeroSum += payload.output[OIL_EUR_WELL_PRODUCTION_TIME_ZERO].oilEurWellProductionTimeZero[j];
            oilActualProductionBarrelsPerDaySum += payload.output[OIL_ACTUAL_PRODUCTION_BARRELS_PER_DAY].oilActualProductionBarrelsPerDay[j];
            oilCumulativeActualProductionSum += payload.output[OIL_CUMULATIVE_ACTUAL_PRODUCTION].oilCumulativeActualProduction[j];
            oilActualProductionMMFCESum += payload.output[OIL_ACTUAL_PRODUCTION_MMFCE].oilActualProductionMMFCE[j];
            WHGasHyperbolicPercentageDeclineSum += payload.output[WH_GAS_HYPERBOLIC_PERCENTAGE_DECLINE].WHGasHyperbolicPercentageDecline[j];
            WHGasMomPercentageDeclineSum += payload.output[WH_GAS_MOM_PERCENTAGE_DECLINE].WHGasMomPercentageDecline[j];
            WHGasTimeZeroUnconstrainedProductionSum += payload.output[WH_GAS_TIME_ZERO_UNCONSTRAINED_PRODUCTION].WHGasTimeZeroUnconstrainedProduction[j];
            WHGasEurWellProductionTimeZeroSum += payload.output[WH_GAS_EUR_WELL_PRODUCTION_TIME_ZERO].WHGasEurWellProductionTimeZero[j];
            WHGasActualProductionMMFCDSum += payload.output[WH_GAS_ACTUAL_PRODUCTION_MMFCD].WHGasActualProductionMMFCD[j];
            WHGasCumulativeActualProductionSum += payload.output[WH_GAS_CUMULATIVE_ACTUAL_PRODUCTION].WHGasCumulativeActualProduction[j];
            NGLTimeZeroUnconstrainedProductionSum += payload.output[NGL_TIME_ZERO_UNCONSTRAINED_PRODUCTION].NGLTimeZeroUnconstrainedProduction[j];
            NGLEurWellProductionTimeZeroSum += payload.output[NGL_EUR_WELL_PRODUCTION_TIME_ZERO].NGLEurWellProductionTimeZero[j];
            NGLActualProductionBarrelsPerDaySum += payload.output[NGL_ACTUAL_PRODUCTION_BARRELS_PER_DAY].NGLActualProductionBarrelsPerDay[j];
            NGLCumulativeActualProductionSum += payload.output[NGL_CUMULATIVE_ACTUAL_PRODUCTION].NGLCumulativeActualProduction[j];
            dryGasTimeZeroUnconstrainedProductionSum += payload.output[DRY_GAS_TIME_ZERO_UNCONSTRAINED_PRODUCTION].dryGasTimeZeroUnconstrainedProduction[j];
            dryGasEurWellProductionTimeZeroSum += payload.output[DRY_GAS_EUR_WELL_PRODUCTION_TIME_ZERO].dryGasEurWellProductionTimeZero[j];
            dryGasActualProductionMMCFEDSum += payload.output[DRY_GAS_ACTUAL_PRODUCTION_MMCFED].dryGasActualProductionMMCFED[j];
            dryGasCumulativeActualProductionSum += payload.output[DRY_GAS_CUMULATIVE_ACTUAL_PRODUCTION].dryGasCumulativeActualProduction[j];
            waterHyperbolicPercentageDeclineSum += payload.output[WATER_HYPERBOLIC_PERCENTAGE_DECLINE].waterHyperbolicPercentageDecline[j];
            waterMomPercentageDeclineSum += payload.output[WATER_MOM_PERCENTAGE_DECLINE].waterMomPercentageDecline[j];
            waterTimeZeroUnconstrainedProductionSum += payload.output[WATER_TIME_ZERO_UNCONSTRAINED_PRODUCTION].waterTimeZeroUnconstrainedProduction[j];
            waterActualProductionBarrelsPerDaySum += payload.output[WATER_ACTUAL_PRODUCTION_BARRELS_PER_DAY].waterActualProductionBarrelsPerDay[j];
            waterCumulativeActualProductionSum += payload.output[WATER_CUMULATIVE_ACTUAL_PRODUCTION].waterCumulativeActualProduction[j];
            totalActualProductionBOEDSum += payload.output[TOTAL_ACTUAL_PRODUCTION_BOED].totalActualProductionBOED[j];
            totalActualproductionMMFCEDSum += payload.output[TOTAL_ACTUAL_PRODUCTION_MMFCED].totalActualproductionMMFCED[j];
            cumulativeTotalProductionMBOESum += payload.output[CUMULATIVE_TOTAL_PRODUCTION_MBOE].cumulativeTotalProductionMBOE[j];
            cumulativeTotalProductionBCFESum += payload.output[CUMULATIVE_TOTAL_PRODUCTION_BCFE].cumulativeTotalProductionBCFE[j];

            //console.log("A: " + j);
            //console.log("Foo: " + payload.output[OIL_ACTUAL_PRODUCTION_BARRELS_PER_DAY].oilActualProductionBarrelsPerDay[j]);
        }

        // responseJson.series.quarterly.quarter.push( `Q${(i-1)%4+1}-${Math.floor(baseYear+(i-1)/4)}` );

        responseJson.series.quarterly.oilHyperbolicPercentageDecline.push(oilHyperbolicPercentageDeclineSum);
        responseJson.series.quarterly.oilMomPercentageDecline.push(oilMomPercentageDeclineSum);
        responseJson.series.quarterly.oilTimeZeroUnconstrainedProduction.push(oilTimeZeroUnconstrainedProductionSum);
        responseJson.series.quarterly.oilEurWellProductionTimeZero.push(oilEurWellProductionTimeZeroSum);
        responseJson.series.quarterly.oilActualProductionBarrelsPerDay.push(oilActualProductionBarrelsPerDaySum);
        responseJson.series.quarterly.oilCumulativeActualProduction.push(oilCumulativeActualProductionSum);
        responseJson.series.quarterly.oilActualProductionMMFCE.push(oilActualProductionMMFCESum);
        responseJson.series.quarterly.WHGasHyperbolicPercentageDecline.push(WHGasHyperbolicPercentageDeclineSum);
        responseJson.series.quarterly.WHGasMomPercentageDecline.push(WHGasMomPercentageDeclineSum);
        responseJson.series.quarterly.WHGasTimeZeroUnconstrainedProduction.push(WHGasTimeZeroUnconstrainedProductionSum);
        responseJson.series.quarterly.WHGasEurWellProductionTimeZero.push(WHGasEurWellProductionTimeZeroSum);
        responseJson.series.quarterly.WHGasActualProductionMMFCD.push(WHGasActualProductionMMFCDSum);
        responseJson.series.quarterly.WHGasCumulativeActualProduction.push(WHGasCumulativeActualProductionSum);
        responseJson.series.quarterly.NGLTimeZeroUnconstrainedProduction.push(NGLTimeZeroUnconstrainedProductionSum);
        responseJson.series.quarterly.NGLEurWellProductionTimeZero.push(NGLEurWellProductionTimeZeroSum);
        responseJson.series.quarterly.NGLActualProductionBarrelsPerDay.push(NGLActualProductionBarrelsPerDaySum);
        responseJson.series.quarterly.NGLCumulativeActualProduction.push(NGLCumulativeActualProductionSum);
        responseJson.series.quarterly.dryGasTimeZeroUnconstrainedProduction.push(dryGasTimeZeroUnconstrainedProductionSum);
        responseJson.series.quarterly.dryGasEurWellProductionTimeZero.push(dryGasEurWellProductionTimeZeroSum);
        responseJson.series.quarterly.dryGasActualProductionMMCFED.push(dryGasActualProductionMMCFEDSum);
        responseJson.series.quarterly.dryGasCumulativeActualProduction.push(dryGasCumulativeActualProductionSum);
        responseJson.series.quarterly.waterHyperbolicPercentageDecline.push(waterHyperbolicPercentageDeclineSum);
        responseJson.series.quarterly.waterMomPercentageDecline.push(waterMomPercentageDeclineSum);
        responseJson.series.quarterly.waterTimeZeroUnconstrainedProduction.push(waterTimeZeroUnconstrainedProductionSum);
        responseJson.series.quarterly.waterActualProductionBarrelsPerDay.push(waterActualProductionBarrelsPerDaySum);
        responseJson.series.quarterly.waterCumulativeActualProduction.push(waterCumulativeActualProductionSum);
        responseJson.series.quarterly.totalActualProductionBOED.push(totalActualProductionBOEDSum);
        responseJson.series.quarterly.totalActualproductionMMFCED.push(totalActualproductionMMFCEDSum);
        responseJson.series.quarterly.cumulativeTotalProductionMBOE.push(cumulativeTotalProductionMBOESum);
        responseJson.series.quarterly.cumulativeTotalProductionBCFE.push(cumulativeTotalProductionBCFESum);

        //console.log("xx");
    }

    return responseJson;
};

getYearlyData = (payload, months, baseYear, responseJson) => {

    const wellLifeMonths = months;
    for (var i=0; i < wellLifeMonths / 12; ++i) {
        //responseJson.series.yearly.year.push( `Q${(i-1)%4+1}-${Math.floor(baseYear+(i-1)/4)}` );
        responseJson.series.yearly.year.push(baseYear + i);
    }

    for (var i=0; i < wellLifeMonths; i += 12) {
        
        let oilHyperbolicPercentageDeclineSum = 0;
        let oilMomPercentageDeclineSum = 0;
        let oilTimeZeroUnconstrainedProductionSum  = 0;
        let oilEurWellProductionTimeZeroSum = 0;
        let oilActualProductionBarrelsPerDaySum = 0;
        let oilCumulativeActualProductionSum = 0;
        let oilActualProductionMMFCESum = 0;
        let WHGasHyperbolicPercentageDeclineSum = 0;
        let WHGasMomPercentageDeclineSum = 0;
        let WHGasTimeZeroUnconstrainedProductionSum = 0;
        let WHGasEurWellProductionTimeZeroSum = 0;
        let WHGasActualProductionMMFCDSum = 0;
        let WHGasCumulativeActualProductionSum = 0;
        let NGLTimeZeroUnconstrainedProductionSum = 0;
        let NGLEurWellProductionTimeZeroSum = 0;
        let NGLActualProductionBarrelsPerDaySum = 0;
        let NGLCumulativeActualProductionSum = 0;
        let dryGasTimeZeroUnconstrainedProductionSum = 0;
        let dryGasEurWellProductionTimeZeroSum = 0;
        let dryGasActualProductionMMCFEDSum = 0;
        let dryGasCumulativeActualProductionSum = 0;
        let waterHyperbolicPercentageDeclineSum = 0;
        let waterMomPercentageDeclineSum = 0;
        let waterTimeZeroUnconstrainedProductionSum = 0;
        let waterActualProductionBarrelsPerDaySum = 0;
        let waterCumulativeActualProductionSum = 0;
        let totalActualProductionBOEDSum = 0;
        let totalActualproductionMMFCEDSum = 0;
        let cumulativeTotalProductionMBOESum = 0;
        let cumulativeTotalProductionBCFESum = 0.0;
        
        for (var j=i; j < i+12; ++j) {
            
            oilHyperbolicPercentageDeclineSum += payload.output[OIL_HYPERBOLIC_PERCENTAGE_DECLINE].oilHyperbolicPercentageDecline[j];
            oilMomPercentageDeclineSum += payload.output[OIL_MOM_PERCENTAGE_DECLINE].oilMomPercentageDecline[j];
            oilTimeZeroUnconstrainedProductionSum += payload.output[OIL_TIME_ZERO_UNCONSTRAINED_PRODUCTION].oilTimeZeroUnconstrainedProduction[j];
            oilEurWellProductionTimeZeroSum += payload.output[OIL_EUR_WELL_PRODUCTION_TIME_ZERO].oilEurWellProductionTimeZero[j];
            oilActualProductionBarrelsPerDaySum += payload.output[OIL_ACTUAL_PRODUCTION_BARRELS_PER_DAY].oilActualProductionBarrelsPerDay[j];
            oilCumulativeActualProductionSum += payload.output[OIL_CUMULATIVE_ACTUAL_PRODUCTION].oilCumulativeActualProduction[j];
            oilActualProductionMMFCESum += payload.output[OIL_ACTUAL_PRODUCTION_MMFCE].oilActualProductionMMFCE[j];
            WHGasHyperbolicPercentageDeclineSum += payload.output[WH_GAS_HYPERBOLIC_PERCENTAGE_DECLINE].WHGasHyperbolicPercentageDecline[j];
            WHGasMomPercentageDeclineSum += payload.output[WH_GAS_MOM_PERCENTAGE_DECLINE].WHGasMomPercentageDecline[j];
            WHGasTimeZeroUnconstrainedProductionSum += payload.output[WH_GAS_TIME_ZERO_UNCONSTRAINED_PRODUCTION].WHGasTimeZeroUnconstrainedProduction[j];
            WHGasEurWellProductionTimeZeroSum += payload.output[WH_GAS_EUR_WELL_PRODUCTION_TIME_ZERO].WHGasEurWellProductionTimeZero[j];
            WHGasActualProductionMMFCDSum += payload.output[WH_GAS_ACTUAL_PRODUCTION_MMFCD].WHGasActualProductionMMFCD[j];
            WHGasCumulativeActualProductionSum += payload.output[WH_GAS_CUMULATIVE_ACTUAL_PRODUCTION].WHGasCumulativeActualProduction[j];
            NGLTimeZeroUnconstrainedProductionSum += payload.output[NGL_TIME_ZERO_UNCONSTRAINED_PRODUCTION].NGLTimeZeroUnconstrainedProduction[j];
            NGLEurWellProductionTimeZeroSum += payload.output[NGL_EUR_WELL_PRODUCTION_TIME_ZERO].NGLEurWellProductionTimeZero[j];
            NGLActualProductionBarrelsPerDaySum += payload.output[NGL_ACTUAL_PRODUCTION_BARRELS_PER_DAY].NGLActualProductionBarrelsPerDay[j];
            NGLCumulativeActualProductionSum += payload.output[NGL_CUMULATIVE_ACTUAL_PRODUCTION].NGLCumulativeActualProduction[j];
            dryGasTimeZeroUnconstrainedProductionSum += payload.output[DRY_GAS_TIME_ZERO_UNCONSTRAINED_PRODUCTION].dryGasTimeZeroUnconstrainedProduction[j];
            dryGasEurWellProductionTimeZeroSum += payload.output[DRY_GAS_EUR_WELL_PRODUCTION_TIME_ZERO].dryGasEurWellProductionTimeZero[j];
            dryGasActualProductionMMCFEDSum += payload.output[DRY_GAS_ACTUAL_PRODUCTION_MMCFED].dryGasActualProductionMMCFED[j];
            dryGasCumulativeActualProductionSum += payload.output[DRY_GAS_CUMULATIVE_ACTUAL_PRODUCTION].dryGasCumulativeActualProduction[j];
            waterHyperbolicPercentageDeclineSum += payload.output[WATER_HYPERBOLIC_PERCENTAGE_DECLINE].waterHyperbolicPercentageDecline[j];
            waterMomPercentageDeclineSum += payload.output[WATER_MOM_PERCENTAGE_DECLINE].waterMomPercentageDecline[j];
            waterTimeZeroUnconstrainedProductionSum += payload.output[WATER_TIME_ZERO_UNCONSTRAINED_PRODUCTION].waterTimeZeroUnconstrainedProduction[j];
            waterActualProductionBarrelsPerDaySum += payload.output[WATER_ACTUAL_PRODUCTION_BARRELS_PER_DAY].waterActualProductionBarrelsPerDay[j];
            waterCumulativeActualProductionSum += payload.output[WATER_CUMULATIVE_ACTUAL_PRODUCTION].waterCumulativeActualProduction[j];
            totalActualProductionBOEDSum += payload.output[TOTAL_ACTUAL_PRODUCTION_BOED].totalActualProductionBOED[j];
            totalActualproductionMMFCEDSum += payload.output[TOTAL_ACTUAL_PRODUCTION_MMFCED].totalActualproductionMMFCED[j];
            cumulativeTotalProductionMBOESum += payload.output[CUMULATIVE_TOTAL_PRODUCTION_MBOE].cumulativeTotalProductionMBOE[j];
            cumulativeTotalProductionBCFESum += payload.output[CUMULATIVE_TOTAL_PRODUCTION_BCFE].cumulativeTotalProductionBCFE[j];
        }

        responseJson.series.yearly.oilHyperbolicPercentageDecline.push(oilHyperbolicPercentageDeclineSum);
        responseJson.series.yearly.oilMomPercentageDecline.push(oilMomPercentageDeclineSum);
        responseJson.series.yearly.oilTimeZeroUnconstrainedProduction.push(oilTimeZeroUnconstrainedProductionSum);
        responseJson.series.yearly.oilEurWellProductionTimeZero.push(oilEurWellProductionTimeZeroSum);
        responseJson.series.yearly.oilActualProductionBarrelsPerDay.push(oilActualProductionBarrelsPerDaySum);
        responseJson.series.yearly.oilCumulativeActualProduction.push(oilCumulativeActualProductionSum);
        responseJson.series.yearly.oilActualProductionMMFCE.push(oilActualProductionMMFCESum);
        responseJson.series.yearly.WHGasHyperbolicPercentageDecline.push(WHGasHyperbolicPercentageDeclineSum);
        responseJson.series.yearly.WHGasMomPercentageDecline.push(WHGasMomPercentageDeclineSum);
        responseJson.series.yearly.WHGasTimeZeroUnconstrainedProduction.push(WHGasTimeZeroUnconstrainedProductionSum);
        responseJson.series.yearly.WHGasEurWellProductionTimeZero.push(WHGasEurWellProductionTimeZeroSum);
        responseJson.series.yearly.WHGasActualProductionMMFCD.push(WHGasActualProductionMMFCDSum);
        responseJson.series.yearly.WHGasCumulativeActualProduction.push(WHGasCumulativeActualProductionSum);
        responseJson.series.yearly.NGLTimeZeroUnconstrainedProduction.push(NGLTimeZeroUnconstrainedProductionSum);
        responseJson.series.yearly.NGLEurWellProductionTimeZero.push(NGLEurWellProductionTimeZeroSum);
        responseJson.series.yearly.NGLActualProductionBarrelsPerDay.push(NGLActualProductionBarrelsPerDaySum);
        responseJson.series.yearly.NGLCumulativeActualProduction.push(NGLCumulativeActualProductionSum);
        responseJson.series.yearly.dryGasTimeZeroUnconstrainedProduction.push(dryGasTimeZeroUnconstrainedProductionSum);
        responseJson.series.yearly.dryGasEurWellProductionTimeZero.push(dryGasEurWellProductionTimeZeroSum);
        responseJson.series.yearly.dryGasActualProductionMMCFED.push(dryGasActualProductionMMCFEDSum);
        responseJson.series.yearly.dryGasCumulativeActualProduction.push(dryGasCumulativeActualProductionSum);
        responseJson.series.yearly.waterHyperbolicPercentageDecline.push(waterHyperbolicPercentageDeclineSum);
        responseJson.series.yearly.waterMomPercentageDecline.push(waterMomPercentageDeclineSum);
        responseJson.series.yearly.waterTimeZeroUnconstrainedProduction.push(waterTimeZeroUnconstrainedProductionSum);
        responseJson.series.yearly.waterActualProductionBarrelsPerDay.push(waterActualProductionBarrelsPerDaySum);
        responseJson.series.yearly.waterCumulativeActualProduction.push(waterCumulativeActualProductionSum);
        responseJson.series.yearly.totalActualProductionBOED.push(totalActualProductionBOEDSum);
        responseJson.series.yearly.totalActualproductionMMFCED.push(totalActualproductionMMFCEDSum);
        responseJson.series.yearly.cumulativeTotalProductionMBOE.push(cumulativeTotalProductionMBOESum);
        responseJson.series.yearly.cumulativeTotalProductionBCFE.push(cumulativeTotalProductionBCFESum);
    }

    return responseJson;
};

getSummaryData = (payload, responseJson) =>
{
    // Total summary data
    responseJson.summary.total.thirtyDayIpOilEquivalent.value = payload.output[THIRTY_DAY_IP_OIL_EQUIVALENT].thirtyDayIpOilEquivalent[0];
    responseJson.summary.total.thirtyDayIpGasEquivalent.value = payload.output[THIRTY_DAY_IP_GAS_EQUIVALENT].thirtyDayIpGasEquivalent[0];
    responseJson.summary.total.waterEUR.value = payload.output[WATER_EUR].waterEUR[0];
    responseJson.summary.total.oilEUR.value = payload.output[OIL_EUR].oilEUR[0];
    responseJson.summary.total.nglEUR.value = payload.output[NGL_EUR].nglEUR[0];
    responseJson.summary.total.dryGasEUR.value = payload.output[DRY_GAS_EUR].dryGasEUR[0];
    responseJson.summary.total.oilEURPercentage.value = payload.output[OIL_EUR_PERCENTAGE].oilEURPercentage[0];
    responseJson.summary.total.nglEURPercentage.value = payload.output[NGL_EUR_PERCENTAGE].nglEURPercentage[0];
    responseJson.summary.total.dryGasPercentage.value = payload.output[DRY_GAS_PERCENTAGE].dryGasPercentage[0];
    responseJson.summary.total.gasShrinkagePercentage.value = payload.output[GAS_SHRINKAGE_PERCENTAGE].gasShrinkagePercentage[0];
    
    // Oil summary data
    responseJson.summary.oil.thirtyDayIp.value = payload.output[OIL_THIRTY_DAY_IP].oilThirtyDayIp[0];
    responseJson.summary.oil.EUR.value = payload.output[OIL_EUR].oilEUR[0];
    responseJson.summary.oil.oilDi.value = payload.output[OIL_DI].oilDi[0];
    responseJson.summary.oil.oilBi.value = payload.output[OIL_BI].oilBi[0];
    responseJson.summary.oil.oilDMin.value = payload.output[OIL_DMIN].oilDMin[0];

    // WH Gas summary data
    responseJson.summary.whGas.thirtyDayIp.value = payload.output[WH_GAS_THIRTY_DAY_IP].whGasThirtyDayIp[0];
    responseJson.summary.whGas.EUR.value = payload.output[WATER_EUR].waterEUR[0];
    responseJson.summary.whGas.whGasDi.value = payload.output[WH_GAS_DI].whGasDi[0];
    responseJson.summary.whGas.whGasBi.value = payload.output[WH_GAS_BI].whGasBi[0];
    responseJson.summary.whGas.whGasDMin.value = payload.output[WH_GAS_DMIN].whGasDMin[0];

    // NGL summary data
    responseJson.summary.ngl.thirtyDayIp.value = payload.output[NGL_THIRTY_DAY_IP].nglThirtyDayIp[0];
    responseJson.summary.ngl.EUR.value = payload.output[NGL_EUR].nglEUR[0];
    responseJson.summary.ngl.nglYield.value = payload.output[NGL_YIELD].nglYield[0];
    responseJson.summary.ngl.gasProcessed.value = payload.output[GAS_PROCESSED].gasProcesssed[0]; // TODO: Fix this spelling after fixing backend
    responseJson.summary.ngl.plantEffectiveness.value = payload.output[PLANT_EFFECTIVENESS].plantEffectiveness[0];

    // Dry gas summary data
    responseJson.summary.dryGas.thirtyDayIp.value = payload.output[DRY_GAS_THIRTY_DAY_IP].dryGasThirtyDayIp[0];
    responseJson.summary.dryGas.EUR.value = payload.output[DRY_GAS_EUR].dryGasEUR[0];
    responseJson.summary.dryGas.shrinkage.value = payload.output[GAS_SHRINKAGE].gasShrinkage[0];
    responseJson.summary.dryGas.gasProcessed.value = payload.output[GAS_PROCESSED].gasProcesssed[0]; // TODO: Fix this spelling after fixing backend
    responseJson.summary.dryGas.plantEffectiveness.value = payload.output[PLANT_EFFECTIVENESS].plantEffectiveness[0];

    // Water summary data
    responseJson.summary.water.thirtyDayIp.value = payload.output[WATER_THIRTY_DAY_IP].waterThirtyDayIp[0];
    responseJson.summary.water.EUR.value = payload.output[WATER_EUR].waterEUR[0];
    responseJson.summary.water.waterDi.value = payload.output[WATER_DI].waterDi[0];
    responseJson.summary.water.waterBi.value = payload.output[WATER_BI].waterBi[0];
    responseJson.summary.water.waterDMin.value = payload.output[WATER_DMIN].waterDMin[0];
}

module.exports = {
    buildResponse
};

// const config = require('./config.json');

// // Constants for accessing the output JSON object

// const THIRTY_DAY_IP_OIL_EQUIVALENT = 0;
// const THIRTY_DAY_IP_GAS_EQUIVALENT = 1;
// const WATER_EUR = 2;
// const OIL_EUR = 3;
// const NGL_EUR = 4;
// const DRY_GAS_EUR = 5;
// const OIL_EUR_PERCENTAGE = 6;
// const NGL_EUR_PERCENTAGE = 7;
// const DRY_GAS_PERCENTAGE = 8;
// const GAS_SHRINKAGE_PERCENTAGE = 9;
// const OIL_THIRTY_DAY_IP = 10;
// const OIL_DI = 11;
// const OIL_BI = 12;
// const OIL_DMIN = 13;
// const WH_GAS_THIRTY_DAY_IP = 14;
// const WH_GAS_DI = 15;
// const WH_GAS_BI = 16;
// const WH_GAS_DMIN = 17;
// const NGL_THIRTY_DAY_IP = 18;
// const NGL_YIELD = 19;
// const DRY_GAS_THIRTY_DAY_IP = 20;
// const GAS_SHRINKAGE = 21;
// const GAS_PROCESSED = 22;
// const PLANT_EFFECTIVENESS = 23;
// const WATER_THIRTY_DAY_IP = 24;
// const WATER_DI = 25;
// const WATER_BI = 26;
// const WATER_DMIN = 27;
// const OIL_HYPERBOLIC_PERCENTAGE_DECLINE = 28;
// const OIL_MOM_PERCENTAGE_DECLINE = 29;
// const OIL_TIME_ZERO_UNCONSTRAINED_PRODUCTION = 30;
// const OIL_EUR_WELL_PRODUCTION_TIME_ZERO = 31;
// const OIL_ACTUAL_PRODUCTION_BARRELS_PER_DAY = 32;
// const OIL_CUMULATIVE_ACTUAL_PRODUCTION = 33;
// const OIL_ACTUAL_PRODUCTION_MMFCE = 34;
// const WH_GAS_HYPERBOLIC_PERCENTAGE_DECLINE = 35;
// const WH_GAS_MOM_PERCENTAGE_DECLINE = 36;
// const WH_GAS_TIME_ZERO_UNCONSTRAINED_PRODUCTION = 37;
// const WH_GAS_EUR_WELL_PRODUCTION_TIME_ZERO = 38;
// const WH_GAS_ACTUAL_PRODUCTION_MMFCD = 39;
// const WH_GAS_CUMULATIVE_ACTUAL_PRODUCTION = 40;
// const NGL_TIME_ZERO_UNCONSTRAINED_PRODUCTION = 41;
// const NGL_EUR_WELL_PRODUCTION_TIME_ZERO = 42;
// const NGL_ACTUAL_PRODUCTION_BARRELS_PER_DAY = 43;
// const NGL_CUMULATIVE_ACTUAL_PRODUCTION = 44;
// const DRY_GAS_TIME_ZERO_UNCONSTRAINED_PRODUCTION = 45;
// const DRY_GAS_EUR_WELL_PRODUCTION_TIME_ZERO = 46;
// const DRY_GAS_ACTUAL_PRODUCTION_MMCFED = 47;
// const DRY_GAS_CUMULATIVE_ACTUAL_PRODUCTION = 48;
// const WATER_HYPERBOLIC_PERCENTAGE_DECLINE = 49;
// const WATER_MOM_PERCENTAGE_DECLINE = 50;
// const WATER_TIME_ZERO_UNCONSTRAINED_PRODUCTION = 51;
// const WATER_ACTUAL_PRODUCTION_BARRELS_PER_DAY = 52;
// const WATER_CUMULATIVE_ACTUAL_PRODUCTION = 53;
// const TOTAL_ACTUAL_PRODUCTION_BOED = 54;
// const TOTAL_ACTUAL_PRODUCTION_MMFCED = 55;
// const CUMULATIVE_TOTAL_PRODUCTION_MBOE = 56;
// const CUMULATIVE_TOTAL_PRODUCTION_BCFE = 57;

// buildResponse = (payload, clientPayload) => {

//     // Load the configuration for series data
//     const seriesConfig = config.series;
//     const summaryConfig = config.summary;

//     const responseJson = {      
//         series: seriesConfig,
//         summary: summaryConfig
//     };

//     const months = clientPayload.production.wellLifeMonths;
//     const quarters = months / 3;
//     const years = months / 12;
//     const baseYear = 2018;

//     getMonthlyData(payload, months, responseJson);
//     getQuarterlyData(payload, months, baseYear, responseJson);
//     getYearlyData(payload, months, baseYear, responseJson);
//     getSummaryData(payload, responseJson);

//     return responseJson;
// };

// getMonthlyData = (payload, months, responseJson) => {

//     const wellLifeMonths = months;
//     var monthlyData = responseJson.series.monthly;
//     // var outputData = payload.output;

//     //responseJson.series.monthly.oilHyperbolicPercentageDecline = payload.output[OIL_HYPERBOLIC_PERCENTAGE_DECLINE].oilHyperbolicPercentageDecline;

//     for (var i=1; i <= wellLifeMonths; ++i) {
//         //responseJson.series.monthly["month"].push(i);
//         //responseJson.series.monthly[Object.keys(monthlyData)[0]].push(i);

//         // for (var property=1; property < 30; ++property) {

//         //     responseJson.series.monthly[Object.keys(monthlyData)[property]].push(payload.output[property+OIL_HYPERBOLIC_PERCENTAGE_DECLINE][Object.keys(monthlyData)[property]][i]);
//         // }
        
//         //responseJson.series.monthly.Object.keys(monthlyData)[1].push(payload.output[OIL_HYPERBOLIC_PERCENTAGE_DECLINE].oilHyperbolicPercentageDecline[i]);
//         monthlyData.month.push(i);
//         monthlyData.oilHyperbolicPercentageDecline.push(payload.output[OIL_HYPERBOLIC_PERCENTAGE_DECLINE].oilHyperbolicPercentageDecline[i]);
//         monthlyData.oilMomPercentageDecline.push(payload.output[OIL_MOM_PERCENTAGE_DECLINE].oilMomPercentageDecline[i]);
//         monthlyData.oilTimeZeroUnconstrainedProduction.push(payload.output[OIL_TIME_ZERO_UNCONSTRAINED_PRODUCTION].oilTimeZeroUnconstrainedProduction[i]);
//         monthlyData.oilEurWellProductionTimeZero.push(payload.output[OIL_EUR_WELL_PRODUCTION_TIME_ZERO].oilEurWellProductionTimeZero[i]);
//         monthlyData.oilActualProductionBarrelsPerDay.push(payload.output[OIL_ACTUAL_PRODUCTION_BARRELS_PER_DAY].oilActualProductionBarrelsPerDay[i]);
//         monthlyData.oilCumulativeActualProduction.push(payload.output[OIL_CUMULATIVE_ACTUAL_PRODUCTION].oilCumulativeActualProduction[i]);
//         monthlyData.oilActualProductionMMFCE.push(payload.output[OIL_ACTUAL_PRODUCTION_MMFCE].oilActualProductionMMFCE[i]);
//         monthlyData.WHGasHyperbolicPercentageDecline.push(payload.output[WH_GAS_HYPERBOLIC_PERCENTAGE_DECLINE].WHGasHyperbolicPercentageDecline[i]);
//         monthlyData.WHGasMomPercentageDecline.push(payload.output[WH_GAS_MOM_PERCENTAGE_DECLINE].WHGasMomPercentageDecline[i]);
//         monthlyData.WHGasTimeZeroUnconstrainedProduction.push(payload.output[WH_GAS_TIME_ZERO_UNCONSTRAINED_PRODUCTION].WHGasTimeZeroUnconstrainedProduction[i]);
//         monthlyData.WHGasEurWellProductionTimeZero.push(payload.output[WH_GAS_EUR_WELL_PRODUCTION_TIME_ZERO].WHGasEurWellProductionTimeZero[i]);
//         monthlyData.WHGasActualProductionMMFCD.push(payload.output[WH_GAS_ACTUAL_PRODUCTION_MMFCD].WHGasActualProductionMMFCD[i]);
//         monthlyData.WHGasCumulativeActualProduction.push(payload.output[WH_GAS_CUMULATIVE_ACTUAL_PRODUCTION].WHGasCumulativeActualProduction[i]);
//         monthlyData.NGLTimeZeroUnconstrainedProduction.push(payload.output[NGL_TIME_ZERO_UNCONSTRAINED_PRODUCTION].NGLTimeZeroUnconstrainedProduction[i]);
//         monthlyData.NGLEurWellProductionTimeZero.push(payload.output[NGL_EUR_WELL_PRODUCTION_TIME_ZERO].NGLEurWellProductionTimeZero[i]);
//         monthlyData.NGLActualProductionBarrelsPerDay.push(payload.output[NGL_ACTUAL_PRODUCTION_BARRELS_PER_DAY].NGLActualProductionBarrelsPerDay[i]);
//         monthlyData.NGLCumulativeActualProduction.push(payload.output[NGL_CUMULATIVE_ACTUAL_PRODUCTION].NGLCumulativeActualProduction[i]);
//         monthlyData.dryGasTimeZeroUnconstrainedProduction.push(payload.output[DRY_GAS_TIME_ZERO_UNCONSTRAINED_PRODUCTION].dryGasTimeZeroUnconstrainedProduction[i]);
//         monthlyData.dryGasEurWellProductionTimeZero.push(payload.output[DRY_GAS_EUR_WELL_PRODUCTION_TIME_ZERO].dryGasEurWellProductionTimeZero[i]);
//         monthlyData.dryGasActualProductionMMCFED.push(payload.output[DRY_GAS_ACTUAL_PRODUCTION_MMCFED].dryGasActualProductionMMCFED[i]);
//         monthlyData.dryGasCumulativeActualProduction.push(payload.output[DRY_GAS_CUMULATIVE_ACTUAL_PRODUCTION].dryGasCumulativeActualProduction[i]);
//         monthlyData.waterHyperbolicPercentageDecline.push(payload.output[WATER_HYPERBOLIC_PERCENTAGE_DECLINE].waterHyperbolicPercentageDecline[i]);
//         monthlyData.waterMomPercentageDecline.push(payload.output[WATER_MOM_PERCENTAGE_DECLINE].waterMomPercentageDecline[i]);
//         monthlyData.waterTimeZeroUnconstrainedProduction.push(payload.output[WATER_TIME_ZERO_UNCONSTRAINED_PRODUCTION].waterTimeZeroUnconstrainedProduction[i]);
//         monthlyData.waterActualProductionBarrelsPerDay.push(payload.output[WATER_ACTUAL_PRODUCTION_BARRELS_PER_DAY].waterActualProductionBarrelsPerDay[i]);
//         monthlyData.waterCumulativeActualProduction.push(payload.output[WATER_CUMULATIVE_ACTUAL_PRODUCTION].waterCumulativeActualProduction[i]);
//         monthlyData.totalActualProductionBOED.push(payload.output[TOTAL_ACTUAL_PRODUCTION_BOED].totalActualProductionBOED[i]);
//         monthlyData.totalActualproductionMMFCED.push(payload.output[TOTAL_ACTUAL_PRODUCTION_MMFCED].totalActualproductionMMFCED[i]);
//         monthlyData.cumulativeTotalProductionMBOE.push(payload.output[CUMULATIVE_TOTAL_PRODUCTION_MBOE].cumulativeTotalProductionMBOE[i]);
//         monthlyData.cumulativeTotalProductionBCFE.push(payload.output[CUMULATIVE_TOTAL_PRODUCTION_BCFE].cumulativeTotalProductionBCFE[i]);
//     }

//     return responseJson;
// };

// getQuarterlyData = (payload, months, baseYear, responseJson) => {

//     const wellLifeMonths = months;
//     for (var i=1; i <= wellLifeMonths / 3; ++i) {
//         responseJson.series.quarterly.quarter.push( `Q${(i-1)%4+1}-${Math.floor(baseYear+(i-1)/4)}` );
//     }

//     for (var i=0; i < wellLifeMonths; i+=3) {
        
//         let oilHyperbolicPercentageDeclineSum = 0;
//         let oilMomPercentageDeclineSum = 0;
//         let oilTimeZeroUnconstrainedProductionSum  = 0;
//         let oilEurWellProductionTimeZeroSum = 0;
//         let oilActualProductionBarrelsPerDaySum = 0;
//         let oilCumulativeActualProductionSum = 0;
//         let oilActualProductionMMFCESum = 0;
//         let WHGasHyperbolicPercentageDeclineSum = 0;
//         let WHGasMomPercentageDeclineSum = 0;
//         let WHGasTimeZeroUnconstrainedProductionSum = 0;
//         let WHGasEurWellProductionTimeZeroSum = 0;
//         let WHGasActualProductionMMFCDSum = 0;
//         let WHGasCumulativeActualProductionSum = 0;
//         let NGLTimeZeroUnconstrainedProductionSum = 0;
//         let NGLEurWellProductionTimeZeroSum = 0;
//         let NGLActualProductionBarrelsPerDaySum = 0;
//         let NGLCumulativeActualProductionSum = 0;
//         let dryGasTimeZeroUnconstrainedProductionSum = 0;
//         let dryGasEurWellProductionTimeZeroSum = 0;
//         let dryGasActualProductionMMCFEDSum = 0;
//         let dryGasCumulativeActualProductionSum = 0;
//         let waterHyperbolicPercentageDeclineSum = 0;
//         let waterMomPercentageDeclineSum = 0;
//         let waterTimeZeroUnconstrainedProductionSum = 0;
//         let waterActualProductionBarrelsPerDaySum = 0;
//         let waterCumulativeActualProductionSum = 0;
//         let totalActualProductionBOEDSum = 0;
//         let totalActualproductionMMFCEDSum = 0;
//         let cumulativeTotalProductionMBOESum = 0;
//         let cumulativeTotalProductionBCFESum = 0;

//         //console.log("B" + i)
        
//         for (var j=i; j < i+3; ++j) {
            
//             oilHyperbolicPercentageDeclineSum += payload.output[OIL_HYPERBOLIC_PERCENTAGE_DECLINE].oilHyperbolicPercentageDecline[j];
//             oilMomPercentageDeclineSum += payload.output[OIL_MOM_PERCENTAGE_DECLINE].oilMomPercentageDecline[j];
//             oilTimeZeroUnconstrainedProductionSum += payload.output[OIL_TIME_ZERO_UNCONSTRAINED_PRODUCTION].oilTimeZeroUnconstrainedProduction[j];
//             oilEurWellProductionTimeZeroSum += payload.output[OIL_EUR_WELL_PRODUCTION_TIME_ZERO].oilEurWellProductionTimeZero[j];
//             oilActualProductionBarrelsPerDaySum += payload.output[OIL_ACTUAL_PRODUCTION_BARRELS_PER_DAY].oilActualProductionBarrelsPerDay[j];
//             oilCumulativeActualProductionSum += payload.output[OIL_CUMULATIVE_ACTUAL_PRODUCTION].oilCumulativeActualProduction[j];
//             oilActualProductionMMFCESum += payload.output[OIL_ACTUAL_PRODUCTION_MMFCE].oilActualProductionMMFCE[j];
//             WHGasHyperbolicPercentageDeclineSum += payload.output[WH_GAS_HYPERBOLIC_PERCENTAGE_DECLINE].WHGasHyperbolicPercentageDecline[j];
//             WHGasMomPercentageDeclineSum += payload.output[WH_GAS_MOM_PERCENTAGE_DECLINE].WHGasMomPercentageDecline[j];
//             WHGasTimeZeroUnconstrainedProductionSum += payload.output[WH_GAS_TIME_ZERO_UNCONSTRAINED_PRODUCTION].WHGasTimeZeroUnconstrainedProduction[j];
//             WHGasEurWellProductionTimeZeroSum += payload.output[WH_GAS_EUR_WELL_PRODUCTION_TIME_ZERO].WHGasEurWellProductionTimeZero[j];
//             WHGasActualProductionMMFCDSum += payload.output[WH_GAS_ACTUAL_PRODUCTION_MMFCD].WHGasActualProductionMMFCD[j];
//             WHGasCumulativeActualProductionSum += payload.output[WH_GAS_CUMULATIVE_ACTUAL_PRODUCTION].WHGasCumulativeActualProduction[j];
//             NGLTimeZeroUnconstrainedProductionSum += payload.output[NGL_TIME_ZERO_UNCONSTRAINED_PRODUCTION].NGLTimeZeroUnconstrainedProduction[j];
//             NGLEurWellProductionTimeZeroSum += payload.output[NGL_EUR_WELL_PRODUCTION_TIME_ZERO].NGLEurWellProductionTimeZero[j];
//             NGLActualProductionBarrelsPerDaySum += payload.output[NGL_ACTUAL_PRODUCTION_BARRELS_PER_DAY].NGLActualProductionBarrelsPerDay[j];
//             NGLCumulativeActualProductionSum += payload.output[NGL_CUMULATIVE_ACTUAL_PRODUCTION].NGLCumulativeActualProduction[j];
//             dryGasTimeZeroUnconstrainedProductionSum += payload.output[DRY_GAS_TIME_ZERO_UNCONSTRAINED_PRODUCTION].dryGasTimeZeroUnconstrainedProduction[j];
//             dryGasEurWellProductionTimeZeroSum += payload.output[DRY_GAS_EUR_WELL_PRODUCTION_TIME_ZERO].dryGasEurWellProductionTimeZero[j];
//             dryGasActualProductionMMCFEDSum += payload.output[DRY_GAS_ACTUAL_PRODUCTION_MMCFED].dryGasActualProductionMMCFED[j];
//             dryGasCumulativeActualProductionSum += payload.output[DRY_GAS_CUMULATIVE_ACTUAL_PRODUCTION].dryGasCumulativeActualProduction[j];
//             waterHyperbolicPercentageDeclineSum += payload.output[WATER_HYPERBOLIC_PERCENTAGE_DECLINE].waterHyperbolicPercentageDecline[j];
//             waterMomPercentageDeclineSum += payload.output[WATER_MOM_PERCENTAGE_DECLINE].waterMomPercentageDecline[j];
//             waterTimeZeroUnconstrainedProductionSum += payload.output[WATER_TIME_ZERO_UNCONSTRAINED_PRODUCTION].waterTimeZeroUnconstrainedProduction[j];
//             waterActualProductionBarrelsPerDaySum += payload.output[WATER_ACTUAL_PRODUCTION_BARRELS_PER_DAY].waterActualProductionBarrelsPerDay[j];
//             waterCumulativeActualProductionSum += payload.output[WATER_CUMULATIVE_ACTUAL_PRODUCTION].waterCumulativeActualProduction[j];
//             totalActualProductionBOEDSum += payload.output[TOTAL_ACTUAL_PRODUCTION_BOED].totalActualProductionBOED[j];
//             totalActualproductionMMFCEDSum += payload.output[TOTAL_ACTUAL_PRODUCTION_MMFCED].totalActualproductionMMFCED[j];
//             cumulativeTotalProductionMBOESum += payload.output[CUMULATIVE_TOTAL_PRODUCTION_MBOE].cumulativeTotalProductionMBOE[j];
//             cumulativeTotalProductionBCFESum += payload.output[CUMULATIVE_TOTAL_PRODUCTION_BCFE].cumulativeTotalProductionBCFE[j];

//             //console.log("A: " + j);
//             //console.log("Foo: " + payload.output[OIL_ACTUAL_PRODUCTION_BARRELS_PER_DAY].oilActualProductionBarrelsPerDay[j]);
//         }

//         // responseJson.series.quarterly.quarter.push( `Q${(i-1)%4+1}-${Math.floor(baseYear+(i-1)/4)}` );

//         responseJson.series.quarterly.oilHyperbolicPercentageDecline.push(oilHyperbolicPercentageDeclineSum);
//         responseJson.series.quarterly.oilMomPercentageDecline.push(oilMomPercentageDeclineSum);
//         responseJson.series.quarterly.oilTimeZeroUnconstrainedProduction.push(oilTimeZeroUnconstrainedProductionSum);
//         responseJson.series.quarterly.oilEurWellProductionTimeZero.push(oilEurWellProductionTimeZeroSum);
//         responseJson.series.quarterly.oilActualProductionBarrelsPerDay.push(oilActualProductionBarrelsPerDaySum);
//         responseJson.series.quarterly.oilCumulativeActualProduction.push(oilCumulativeActualProductionSum);
//         responseJson.series.quarterly.oilActualProductionMMFCE.push(oilActualProductionMMFCESum);
//         responseJson.series.quarterly.WHGasHyperbolicPercentageDecline.push(WHGasHyperbolicPercentageDeclineSum);
//         responseJson.series.quarterly.WHGasMomPercentageDecline.push(WHGasMomPercentageDeclineSum);
//         responseJson.series.quarterly.WHGasTimeZeroUnconstrainedProduction.push(WHGasTimeZeroUnconstrainedProductionSum);
//         responseJson.series.quarterly.WHGasEurWellProductionTimeZero.push(WHGasEurWellProductionTimeZeroSum);
//         responseJson.series.quarterly.WHGasActualProductionMMFCD.push(WHGasActualProductionMMFCDSum);
//         responseJson.series.quarterly.WHGasCumulativeActualProduction.push(WHGasCumulativeActualProductionSum);
//         responseJson.series.quarterly.NGLTimeZeroUnconstrainedProduction.push(NGLTimeZeroUnconstrainedProductionSum);
//         responseJson.series.quarterly.NGLEurWellProductionTimeZero.push(NGLEurWellProductionTimeZeroSum);
//         responseJson.series.quarterly.NGLActualProductionBarrelsPerDay.push(NGLActualProductionBarrelsPerDaySum);
//         responseJson.series.quarterly.NGLCumulativeActualProduction.push(NGLCumulativeActualProductionSum);
//         responseJson.series.quarterly.dryGasTimeZeroUnconstrainedProduction.push(dryGasTimeZeroUnconstrainedProductionSum);
//         responseJson.series.quarterly.dryGasEurWellProductionTimeZero.push(dryGasEurWellProductionTimeZeroSum);
//         responseJson.series.quarterly.dryGasActualProductionMMCFED.push(dryGasActualProductionMMCFEDSum);
//         responseJson.series.quarterly.dryGasCumulativeActualProduction.push(dryGasCumulativeActualProductionSum);
//         responseJson.series.quarterly.waterHyperbolicPercentageDecline.push(waterHyperbolicPercentageDeclineSum);
//         responseJson.series.quarterly.waterMomPercentageDecline.push(waterMomPercentageDeclineSum);
//         responseJson.series.quarterly.waterTimeZeroUnconstrainedProduction.push(waterTimeZeroUnconstrainedProductionSum);
//         responseJson.series.quarterly.waterActualProductionBarrelsPerDay.push(waterActualProductionBarrelsPerDaySum);
//         responseJson.series.quarterly.waterCumulativeActualProduction.push(waterCumulativeActualProductionSum);
//         responseJson.series.quarterly.totalActualProductionBOED.push(totalActualProductionBOEDSum);
//         responseJson.series.quarterly.totalActualproductionMMFCED.push(totalActualproductionMMFCEDSum);
//         responseJson.series.quarterly.cumulativeTotalProductionMBOE.push(cumulativeTotalProductionMBOESum);
//         responseJson.series.quarterly.cumulativeTotalProductionBCFE.push(cumulativeTotalProductionBCFESum);

//         //console.log("xx");
//     }

//     return responseJson;
// };

// getYearlyData = (payload, months, baseYear, responseJson) => {

//     const wellLifeMonths = months;
//     for (var i=0; i < wellLifeMonths / 12; ++i) {
//         //responseJson.series.yearly.year.push( `Q${(i-1)%4+1}-${Math.floor(baseYear+(i-1)/4)}` );
//         responseJson.series.yearly.year.push(baseYear + i);
//     }

//     for (var i=0; i < wellLifeMonths; i += 12) {
        
//         let oilHyperbolicPercentageDeclineSum = 0;
//         let oilMomPercentageDeclineSum = 0;
//         let oilTimeZeroUnconstrainedProductionSum  = 0;
//         let oilEurWellProductionTimeZeroSum = 0;
//         let oilActualProductionBarrelsPerDaySum = 0;
//         let oilCumulativeActualProductionSum = 0;
//         let oilActualProductionMMFCESum = 0;
//         let WHGasHyperbolicPercentageDeclineSum = 0;
//         let WHGasMomPercentageDeclineSum = 0;
//         let WHGasTimeZeroUnconstrainedProductionSum = 0;
//         let WHGasEurWellProductionTimeZeroSum = 0;
//         let WHGasActualProductionMMFCDSum = 0;
//         let WHGasCumulativeActualProductionSum = 0;
//         let NGLTimeZeroUnconstrainedProductionSum = 0;
//         let NGLEurWellProductionTimeZeroSum = 0;
//         let NGLActualProductionBarrelsPerDaySum = 0;
//         let NGLCumulativeActualProductionSum = 0;
//         let dryGasTimeZeroUnconstrainedProductionSum = 0;
//         let dryGasEurWellProductionTimeZeroSum = 0;
//         let dryGasActualProductionMMCFEDSum = 0;
//         let dryGasCumulativeActualProductionSum = 0;
//         let waterHyperbolicPercentageDeclineSum = 0;
//         let waterMomPercentageDeclineSum = 0;
//         let waterTimeZeroUnconstrainedProductionSum = 0;
//         let waterActualProductionBarrelsPerDaySum = 0;
//         let waterCumulativeActualProductionSum = 0;
//         let totalActualProductionBOEDSum = 0;
//         let totalActualproductionMMFCEDSum = 0;
//         let cumulativeTotalProductionMBOESum = 0;
//         let cumulativeTotalProductionBCFESum = 0.0;
        
//         for (var j=i; j < i+12; ++j) {
            
//             oilHyperbolicPercentageDeclineSum += payload.output[OIL_HYPERBOLIC_PERCENTAGE_DECLINE].oilHyperbolicPercentageDecline[j];
//             oilMomPercentageDeclineSum += payload.output[OIL_MOM_PERCENTAGE_DECLINE].oilMomPercentageDecline[j];
//             oilTimeZeroUnconstrainedProductionSum += payload.output[OIL_TIME_ZERO_UNCONSTRAINED_PRODUCTION].oilTimeZeroUnconstrainedProduction[j];
//             oilEurWellProductionTimeZeroSum += payload.output[OIL_EUR_WELL_PRODUCTION_TIME_ZERO].oilEurWellProductionTimeZero[j];
//             oilActualProductionBarrelsPerDaySum += payload.output[OIL_ACTUAL_PRODUCTION_BARRELS_PER_DAY].oilActualProductionBarrelsPerDay[j];
//             oilCumulativeActualProductionSum += payload.output[OIL_CUMULATIVE_ACTUAL_PRODUCTION].oilCumulativeActualProduction[j];
//             oilActualProductionMMFCESum += payload.output[OIL_ACTUAL_PRODUCTION_MMFCE].oilActualProductionMMFCE[j];
//             WHGasHyperbolicPercentageDeclineSum += payload.output[WH_GAS_HYPERBOLIC_PERCENTAGE_DECLINE].WHGasHyperbolicPercentageDecline[j];
//             WHGasMomPercentageDeclineSum += payload.output[WH_GAS_MOM_PERCENTAGE_DECLINE].WHGasMomPercentageDecline[j];
//             WHGasTimeZeroUnconstrainedProductionSum += payload.output[WH_GAS_TIME_ZERO_UNCONSTRAINED_PRODUCTION].WHGasTimeZeroUnconstrainedProduction[j];
//             WHGasEurWellProductionTimeZeroSum += payload.output[WH_GAS_EUR_WELL_PRODUCTION_TIME_ZERO].WHGasEurWellProductionTimeZero[j];
//             WHGasActualProductionMMFCDSum += payload.output[WH_GAS_ACTUAL_PRODUCTION_MMFCD].WHGasActualProductionMMFCD[j];
//             WHGasCumulativeActualProductionSum += payload.output[WH_GAS_CUMULATIVE_ACTUAL_PRODUCTION].WHGasCumulativeActualProduction[j];
//             NGLTimeZeroUnconstrainedProductionSum += payload.output[NGL_TIME_ZERO_UNCONSTRAINED_PRODUCTION].NGLTimeZeroUnconstrainedProduction[j];
//             NGLEurWellProductionTimeZeroSum += payload.output[NGL_EUR_WELL_PRODUCTION_TIME_ZERO].NGLEurWellProductionTimeZero[j];
//             NGLActualProductionBarrelsPerDaySum += payload.output[NGL_ACTUAL_PRODUCTION_BARRELS_PER_DAY].NGLActualProductionBarrelsPerDay[j];
//             NGLCumulativeActualProductionSum += payload.output[NGL_CUMULATIVE_ACTUAL_PRODUCTION].NGLCumulativeActualProduction[j];
//             dryGasTimeZeroUnconstrainedProductionSum += payload.output[DRY_GAS_TIME_ZERO_UNCONSTRAINED_PRODUCTION].dryGasTimeZeroUnconstrainedProduction[j];
//             dryGasEurWellProductionTimeZeroSum += payload.output[DRY_GAS_EUR_WELL_PRODUCTION_TIME_ZERO].dryGasEurWellProductionTimeZero[j];
//             dryGasActualProductionMMCFEDSum += payload.output[DRY_GAS_ACTUAL_PRODUCTION_MMCFED].dryGasActualProductionMMCFED[j];
//             dryGasCumulativeActualProductionSum += payload.output[DRY_GAS_CUMULATIVE_ACTUAL_PRODUCTION].dryGasCumulativeActualProduction[j];
//             waterHyperbolicPercentageDeclineSum += payload.output[WATER_HYPERBOLIC_PERCENTAGE_DECLINE].waterHyperbolicPercentageDecline[j];
//             waterMomPercentageDeclineSum += payload.output[WATER_MOM_PERCENTAGE_DECLINE].waterMomPercentageDecline[j];
//             waterTimeZeroUnconstrainedProductionSum += payload.output[WATER_TIME_ZERO_UNCONSTRAINED_PRODUCTION].waterTimeZeroUnconstrainedProduction[j];
//             waterActualProductionBarrelsPerDaySum += payload.output[WATER_ACTUAL_PRODUCTION_BARRELS_PER_DAY].waterActualProductionBarrelsPerDay[j];
//             waterCumulativeActualProductionSum += payload.output[WATER_CUMULATIVE_ACTUAL_PRODUCTION].waterCumulativeActualProduction[j];
//             totalActualProductionBOEDSum += payload.output[TOTAL_ACTUAL_PRODUCTION_BOED].totalActualProductionBOED[j];
//             totalActualproductionMMFCEDSum += payload.output[TOTAL_ACTUAL_PRODUCTION_MMFCED].totalActualproductionMMFCED[j];
//             cumulativeTotalProductionMBOESum += payload.output[CUMULATIVE_TOTAL_PRODUCTION_MBOE].cumulativeTotalProductionMBOE[j];
//             cumulativeTotalProductionBCFESum += payload.output[CUMULATIVE_TOTAL_PRODUCTION_BCFE].cumulativeTotalProductionBCFE[j];
//         }

//         responseJson.series.yearly.oilHyperbolicPercentageDecline.push(oilHyperbolicPercentageDeclineSum);
//         responseJson.series.yearly.oilMomPercentageDecline.push(oilMomPercentageDeclineSum);
//         responseJson.series.yearly.oilTimeZeroUnconstrainedProduction.push(oilTimeZeroUnconstrainedProductionSum);
//         responseJson.series.yearly.oilEurWellProductionTimeZero.push(oilEurWellProductionTimeZeroSum);
//         responseJson.series.yearly.oilActualProductionBarrelsPerDay.push(oilActualProductionBarrelsPerDaySum);
//         responseJson.series.yearly.oilCumulativeActualProduction.push(oilCumulativeActualProductionSum);
//         responseJson.series.yearly.oilActualProductionMMFCE.push(oilActualProductionMMFCESum);
//         responseJson.series.yearly.WHGasHyperbolicPercentageDecline.push(WHGasHyperbolicPercentageDeclineSum);
//         responseJson.series.yearly.WHGasMomPercentageDecline.push(WHGasMomPercentageDeclineSum);
//         responseJson.series.yearly.WHGasTimeZeroUnconstrainedProduction.push(WHGasTimeZeroUnconstrainedProductionSum);
//         responseJson.series.yearly.WHGasEurWellProductionTimeZero.push(WHGasEurWellProductionTimeZeroSum);
//         responseJson.series.yearly.WHGasActualProductionMMFCD.push(WHGasActualProductionMMFCDSum);
//         responseJson.series.yearly.WHGasCumulativeActualProduction.push(WHGasCumulativeActualProductionSum);
//         responseJson.series.yearly.NGLTimeZeroUnconstrainedProduction.push(NGLTimeZeroUnconstrainedProductionSum);
//         responseJson.series.yearly.NGLEurWellProductionTimeZero.push(NGLEurWellProductionTimeZeroSum);
//         responseJson.series.yearly.NGLActualProductionBarrelsPerDay.push(NGLActualProductionBarrelsPerDaySum);
//         responseJson.series.yearly.NGLCumulativeActualProduction.push(NGLCumulativeActualProductionSum);
//         responseJson.series.yearly.dryGasTimeZeroUnconstrainedProduction.push(dryGasTimeZeroUnconstrainedProductionSum);
//         responseJson.series.yearly.dryGasEurWellProductionTimeZero.push(dryGasEurWellProductionTimeZeroSum);
//         responseJson.series.yearly.dryGasActualProductionMMCFED.push(dryGasActualProductionMMCFEDSum);
//         responseJson.series.yearly.dryGasCumulativeActualProduction.push(dryGasCumulativeActualProductionSum);
//         responseJson.series.yearly.waterHyperbolicPercentageDecline.push(waterHyperbolicPercentageDeclineSum);
//         responseJson.series.yearly.waterMomPercentageDecline.push(waterMomPercentageDeclineSum);
//         responseJson.series.yearly.waterTimeZeroUnconstrainedProduction.push(waterTimeZeroUnconstrainedProductionSum);
//         responseJson.series.yearly.waterActualProductionBarrelsPerDay.push(waterActualProductionBarrelsPerDaySum);
//         responseJson.series.yearly.waterCumulativeActualProduction.push(waterCumulativeActualProductionSum);
//         responseJson.series.yearly.totalActualProductionBOED.push(totalActualProductionBOEDSum);
//         responseJson.series.yearly.totalActualproductionMMFCED.push(totalActualproductionMMFCEDSum);
//         responseJson.series.yearly.cumulativeTotalProductionMBOE.push(cumulativeTotalProductionMBOESum);
//         responseJson.series.yearly.cumulativeTotalProductionBCFE.push(cumulativeTotalProductionBCFESum);
//     }

//     return responseJson;
// };

// getSummaryData = (payload, responseJson) =>
// {
//     // Total summary data
//     responseJson.summary.total.thirtyDayIpOilEquivalent.value = payload.output[THIRTY_DAY_IP_OIL_EQUIVALENT].thirtyDayIpOilEquivalent[0];
//     responseJson.summary.total.thirtyDayIpGasEquivalent.value = payload.output[THIRTY_DAY_IP_GAS_EQUIVALENT].thirtyDayIpGasEquivalent[0];
//     responseJson.summary.total.waterEUR.value = payload.output[WATER_EUR].waterEUR[0];
//     responseJson.summary.total.oilEUR.value = payload.output[OIL_EUR].oilEUR[0];
//     responseJson.summary.total.nglEUR.value = payload.output[NGL_EUR].nglEUR[0];
//     responseJson.summary.total.dryGasEUR.value = payload.output[DRY_GAS_EUR].dryGasEUR[0];
//     responseJson.summary.total.oilEURPercentage.value = payload.output[OIL_EUR_PERCENTAGE].oilEURPercentage[0];
//     responseJson.summary.total.nglEURPercentage.value = payload.output[NGL_EUR_PERCENTAGE].nglEURPercentage[0];
//     responseJson.summary.total.dryGasPercentage.value = payload.output[DRY_GAS_PERCENTAGE].dryGasPercentage[0];
//     responseJson.summary.total.gasShrinkagePercentage.value = payload.output[GAS_SHRINKAGE_PERCENTAGE].gasShrinkagePercentage[0];
    
//     // Oil summary data
//     responseJson.summary.oil.thirtyDayIp.value = payload.output[OIL_THIRTY_DAY_IP].oilThirtyDayIp[0];
//     responseJson.summary.oil.EUR.value = payload.output[OIL_EUR].oilEUR[0];
//     responseJson.summary.oil.oilDi.value = payload.output[OIL_DI].oilDi[0];
//     responseJson.summary.oil.oilBi.value = payload.output[OIL_BI].oilBi[0];
//     responseJson.summary.oil.oilDMin.value = payload.output[OIL_DMIN].oilDMin[0];

//     // WH Gas summary data
//     responseJson.summary.whGas.thirtyDayIp.value = payload.output[WH_GAS_THIRTY_DAY_IP].whGasThirtyDayIp[0];
//     responseJson.summary.whGas.EUR.value = payload.output[WATER_EUR].waterEUR[0];
//     responseJson.summary.whGas.whGasDi.value = payload.output[WH_GAS_DI].whGasDi[0];
//     responseJson.summary.whGas.whGasBi.value = payload.output[WH_GAS_BI].whGasBi[0];
//     responseJson.summary.whGas.whGasDMin.value = payload.output[WH_GAS_DMIN].whGasDMin[0];

//     // NGL summary data
//     responseJson.summary.ngl.thirtyDayIp.value = payload.output[NGL_THIRTY_DAY_IP].nglThirtyDayIp[0];
//     responseJson.summary.ngl.EUR.value = payload.output[NGL_EUR].nglEUR[0];
//     responseJson.summary.ngl.nglYield.value = payload.output[NGL_YIELD].nglYield[0];
//     responseJson.summary.ngl.gasProcessed.value = payload.output[GAS_PROCESSED].gasProcesssed[0]; // TODO: Fix this spelling after fixing backend
//     responseJson.summary.ngl.plantEffectiveness.value = payload.output[PLANT_EFFECTIVENESS].plantEffectiveness[0];

//     // Dry gas summary data
//     responseJson.summary.dryGas.thirtyDayIp.value = payload.output[DRY_GAS_THIRTY_DAY_IP].dryGasThirtyDayIp[0];
//     responseJson.summary.dryGas.EUR.value = payload.output[DRY_GAS_EUR].dryGasEUR[0];
//     responseJson.summary.dryGas.shrinkage.value = payload.output[GAS_SHRINKAGE].gasShrinkage[0];
//     responseJson.summary.dryGas.gasProcessed.value = payload.output[GAS_PROCESSED].gasProcesssed[0]; // TODO: Fix this spelling after fixing backend
//     responseJson.summary.dryGas.plantEffectiveness.value = payload.output[PLANT_EFFECTIVENESS].plantEffectiveness[0];

//     // Water summary data
//     responseJson.summary.water.thirtyDayIp.value = payload.output[WATER_THIRTY_DAY_IP].waterThirtyDayIp[0];
//     responseJson.summary.water.EUR.value = payload.output[WATER_EUR].waterEUR[0];
//     responseJson.summary.water.waterDi.value = payload.output[WATER_DI].waterDi[0];
//     responseJson.summary.water.waterBi.value = payload.output[WATER_BI].waterBi[0];
//     responseJson.summary.water.waterDMin.value = payload.output[WATER_DMIN].waterDMin[0];
// }

// module.exports = {
//     buildResponse
// };