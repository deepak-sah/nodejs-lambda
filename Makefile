.Phony: build deploy build_and_deploy

STACK_NAME?=wmap-valuations-api-service
S3_BUCKET?=wmap-app-dev-api
S3_BUCKET_PATH=valuations-api

build:
	docker-compose run --rm --entrypoint=/bin/bash -p 8088:8088 app -c 'npm install \
	&& sam package \
		--template-file template.yaml \
		--s3-bucket ${S3_BUCKET} \
		--s3-prefix ${S3_BUCKET_PATH} \
		--force-upload \
		--output-template-file compiled-template.yaml'

deploy:
	docker-compose run -w /tmp --rm --entrypoint=/bin/bash aws -c 'sam deploy \
		--template-file compiled-template.yaml \
		--capabilities CAPABILITY_IAM \
		--stack-name ${STACK_NAME}'

build_and_deploy: build deploy