/**
 * HTTP request and response related constants
 */

// Status codes
const StatusCodes = {
    OK: 200,
    NotFound: 404,
    InternalServerError: 500,
    BadRequest: 400
}

// Content type
const Headers = {
    ContentType: 'application/json',
    AccessControlAllowOrigin: '*',
    AccessControlAllowMethods: 'GET,OPTIONS,POST',
    AccessControlAllowHeaders: 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,Access-Control-Allow-Origin,Access-Control-Allow-Headers'
};

// Responses
const Responses = {
    NotFound: 'Route not found',
    BadRequest: 'Invalid input JSON',
    InternalServerError: 'An internal server error occurred'
}

module.exports = {
    StatusCodes: StatusCodes,
    Headers: Headers,
    Responses: Responses
};