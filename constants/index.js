const HttpConstants = require('./http-constants');
const JsonConstants = require('./json-constants');

module.exports = {
    Http: HttpConstants,
    Json: JsonConstants
};